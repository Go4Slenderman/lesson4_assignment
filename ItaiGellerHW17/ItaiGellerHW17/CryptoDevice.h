#pragma once

#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>

using namespace std;


class CryptoDevice
{

public:
	CryptoDevice(); //Ctor
	~CryptoDevice(); //Dtor
	string encryptAES(string); //Encrypts a string with AES
	string decryptAES(string); //Decrypts a string with AES

};
