#include "Client.h"

CryptoDevice cryptoDevice;

int process_client(client_type &new_client)
{
	while (1)
	{
		memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{


			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

			string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
			string prefix = strMessage.substr(0, position);
			string postfix = strMessage.substr(position);
			string decrypted_message;

			//this is the only notification we use right now :(
			if (postfix != "Disconnected")
				//please decrypt this part!
				postfix = cryptoDevice.decryptAES(postfix);
			else
				//dont decrypt this - not classified and has not been ecrypted! 
				//trying to do so may cause errors
				decrypted_message = postfix;

			if (iResult != SOCKET_ERROR)
				std::cout << prefix + postfix << endl;
			else
			{
				std::cout << "recv() failed: " << WSAGetLastError() << endl;
				break;
			}
		}
	}

	if (WSAGetLastError() == WSAECONNRESET)
		std::cout << "The server has disconnected" << endl;

	return 0;
}

int main()
{
	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	string sent_message = "";
	client_type client = { INVALID_SOCKET, -1, "" };
	int iResult = 0;
	string message;

	std::cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
		std::cout << "WSAStartup() failed with error: " << iResult << endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	std::cout << "Connecting...\n";

	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		std::cout << "getaddrinfo() failed with error: " << iResult << endl;
		WSACleanup();
		std::system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
			std::cout << "socket() failed with error: " << WSAGetLastError() << endl;
			WSACleanup();
			std::system("pause");
			return 1;
		}

		// Connect to server.
		iResult = connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
		std::cout << "Unable to connect to server!" << endl;
		WSACleanup();
		std::system("pause");
		return 1;
	}

	std::cout << "Successfully Connected" << endl;

	//Obtain id from server for this client;
	recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		//The concept is that every agent will have a different exe file with different credentials on it. This code is an example of a single username and password to demonstrate how the hashing works
		string username = "";
		string password = "";
		CryptoPP::Weak::MD5 hash;
		CryptoPP::HexEncoder encoder;
		string output = "";
		byte digest[CryptoPP::Weak::MD5::DIGESTSIZE];

		for (Uint triesCounter = 0; true; triesCounter++) //3 Tries to connect
		{
			if (triesCounter >= 3) //If the user failed to login
			{
				std::cout << "You're not getting this account thief!" << endl;
				std::system("pause");
				exit(0);
			}
			std::cout << "Please insert your username:" << endl; //Reading the user's credentials
			cin >> username;
			std::cout << "Please insert your password:" << endl;
			cin >> password;
			hash.CalculateDigest(digest, (const byte*)password.c_str(), password.length()); //Getting the hash digest of the password
			encoder.Attach(new CryptoPP::StringSink(output)); //Setting the digest's output to the output string
			encoder.Put(digest, sizeof(digest)); //Transfering the digest to the output string
			encoder.MessageEnd();
			if (username == USER_NAME && output == PASSWORD) //If the credentials are right (the defines are located in Client.h)
			{
				std::cout << "Successfully logged in!" << endl;
				break;
			}
			else
				cout << "Wrong username / password, Please try again." << endl;
		}

		client.id = atoi(client.received_message);

		thread my_thread(process_client, client);

		while (1)
		{
			getline(cin, sent_message);

			//top secret! please encrypt
			string cipher = cryptoDevice.encryptAES(sent_message);

			iResult = send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
				std::cout << "send() failed: " << WSAGetLastError() << endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
		std::cout << client.received_message << endl;

	std::cout << "Shutting down socket..." << endl;
	iResult = shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		std::cout << "shutdown() failed with error: " << WSAGetLastError() << endl;
		closesocket(client.socket);
		WSACleanup();
		std::system("pause");
		return 1;
	}

	closesocket(client.socket);
	WSACleanup();
	std::system("pause");
	return 0;
}