#pragma once

#include "CryptoDevice.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include <thread>
#include "hex.h"
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512            
#define IP_ADDRESS "127.0.0.1"
#define DEFAULT_PORT "8202"

typedef unsigned int Uint;
#define PASSWORD "AFA4E083C9ACEBF82513DE999654DCA0" //This is the hash digest's value of the password - $uper$ecret
#define USER_NAME "agent007"

struct client_type
{
	SOCKET socket;
	int id;
	char received_message[DEFAULT_BUFLEN];
};

class Client
{
public:
	Client();
	~Client();
	int main();

private:
	int process_client(client_type  &new_client);
};

